<?php

namespace Brunoocto\Vmodel\Tests;

use Orchestra\Testbench\TestCase as TestbenchCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Brunoocto\Sample\Tests\SQLiteTestingConnector;
use CloudCreativity\LaravelJsonApi\ServiceProvider;
use Brunoocto\Json\Providers\JsonServiceProvider;
use Brunoocto\Sample\Providers\SampleServiceProvider;
use Brunoocto\Vmodel\Providers\VmodelServiceProvider;
use Brunoocto\Exception\Providers\ExceptionServiceProvider;
use Brunoocto\Filesystem\Providers\FilesystemServiceProvider;
use Brunoocto\Sample\Models\ProjectManagement\Users;

/**
 * TestCase for Service Provider
 */
class TestCase extends TestbenchCase
{
    /**
     * We cannot use refreshDatabase in memory here because of the following bugs:
     * 1) In the same "test" method, if we call more than once a request ->json(), any more call will fail because of some require_once in the Laravel boostrap. So we use refreshApplication();
     * 2) We can use $this->refreshApplication(), but it does launch the transaction rollback, so refreshDatabase cannot be used. So we use DatabaseMigrations.
     * 3) If we use DatabaseMigrations with Memory, refreshApplication delete the database, but not file version. So we use file version.
     */
    use DatabaseMigrations;

    /**
     * Setup launched at every test method
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        // Load fake data to the Database
        $this->withFactories(__DIR__.'/../database/factories');
    }

    /**
     * Set a DB Authenticated Users based on Sample project
     *
     * @return mixed Authenticated User instance
     */
    protected function setAuthenticatedUser()
    {
        // Set Authenticated user
        $this->app['config']->set('auth.defaults.guard', 'vmodel');
        $this->app['config']->set('auth.providers.vmodel.model', Users::class);
        // Instantiate at least one user considerate as Authenticated user
        $user = factory(Users::class)->create();
        // Simulate to have the header "User-Id"
        request()->headers->set('user-id', $user->id);
        // Return the User
        return $user;
    }

    /**
     * Get package providers.
     * @param  Illuminate\Foundation\Application  $app
     * @return array
     */
    protected function getPackageProviders($app)
    {
        return [
            SampleServiceProvider::class,
            ExceptionServiceProvider::class,
            VmodelServiceProvider::class,
            JsonServiceProvider::class,
            FilesystemServiceProvider::class,
            ServiceProvider::class,
        ];
    }

    /**
      * Initialize environment
      * @param mixed $app
      * @return void
      */
    protected function getEnvironmentSetup($app)
    {
        /**
         *  To enable the memory database not constantly refreshing, we overwrite the registrering process of ":memory:" to keep one single connection after application refresh call.
         * using ":shared-memory:" does not work as describe in the below link because of SQLiteBuilder@dropAllTables which works only with ":memory:".
         * https://qiita.com/crhg/items/c53e9381f6c976f211c1
         */
        $app->singleton('db.connector.sqlite', SQLiteTestingConnector::class);

        // Configure Temporary Test database (note that it can be slow to run all tests within big project, but optimizations are possible)
        $app['config']->set('database.default', 'brunoocto_sample');
        $app['config']->set('database.connections.brunoocto_sample', [
            'driver' => 'sqlite',
            'database' => ':memory:',
            'prefix' => 'sample_',
            'foreign_key_constraints' => !is_null(env('LINCKO_SAMPLE_DB_FOREIGN_KEYS')) ? env('LINCKO_SAMPLE_DB_FOREIGN_KEYS') : env('DB_FOREIGN_KEYS', false),
        ]);
    }

    /**
      * Initialize Aliases
      * @param mixed $app
      * @return array
      */
    protected function getPackageAliases($app)
    {
        return [
            'LinckoVmodel' => 'Brunoocto\Vmodel\Facades\VmodelFacade',
            'LinckoJson' => 'Brunoocto\Json\Facades\JsonFacade',
            'JsonApi' => 'CloudCreativity\LaravelJsonApi\Facades\JsonApi',
        ];
    }
}
