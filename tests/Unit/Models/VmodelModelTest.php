<?php

namespace Brunoocto\Vmodel\Tests\Unit\Models;

use Brunoocto\Vmodel\Tests\TestCase;
use Brunoocto\Vmodel\Models\VmodelModel;
use Brunoocto\Sample\Models\ProjectManagement\Projects;
use Exception;

// Set the class Salers for test (without DB)
class Salers extends VmodelModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'salers';
}

// Set the class Cars for test (without DB)
class Cars extends VmodelModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cars';

    /**
    * All relationships
    *
    * @var array
    */
    protected static $model_relationships = [
        // Editable (usually UP relation)
        'salers' => [ // Display name (always plural)
            true, // Authorize input
            true, // Visible
            'salers', // Method name
            'belongsTo', // Relation type
            Salers::class, // Class name
        ],
    ];

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        // Force morphClass to be a Class path
        $this->morphClass = get_class($this);

        parent::__construct($attributes);
    }
}

// Set the class Cruds to test low CRUD permission
class Cruds extends Projects
{
    /**
     * Update crud value
     *
     *  @var array
     */
    public function setCrud(string $crud)
    {
        $this->crud = $crud;
    }

    /**
     * Update crud value
     *
     *  @var array
     */
    public function setCrudOwner(string $crud_owner)
    {
        $this->crud_owner = $crud_owner;
    }
}

/**
 * We just test all lines and methods that are not covered in Features tests
 */
class VmodelModelTest extends TestCase
{
    /**
     * Test getRelationType
     *
     * @return void
     */
    public function testGetRelationType()
    {
        $test = Cars::getRelationType('dummy');
        $this->assertNull($test);
    }

    /**
     * Test getMorphClass
     *
     * @return void
     */
    public function testGetMorphClass()
    {
        $car = new Cars;
        $test = $car->getMorphClass();
        // It should return a table name instead of a class name
        $this->assertEquals($test, 'cars');
    }

    /**
     * Test getDirty
     *
     * @return void
     */
    public function testGetDirty()
    {
        // W use Projects because it has a table with columns
        $project = new Projects;
        $project->title = 'Some project';
        // Create a property that does not exist in DB
        $project->dummy = 'Some dummy content';
        
        // Get modified columns that really exists on DB only
        $test = $project->getDirty();
        $this->assertArrayHasKey('title', $test);
        $this->assertArrayNotHasKey('dummy', $test);
    }

    /**
     * Test getAttributes
     *
     * @return void
     */
    public function testGetAttributes()
    {
        // Set Authenticated user
        $this->setAuthenticatedUser();

        // W use Projects because it has a table with columns
        $project = new Projects;
        $project->title = 'Some project';
        // Create a property that does not exist in DB
        $project->dummy = 'Some dummy content';

        // Check that unknown attributes still exists in the object before saving
        $test = $project->getAttributes();
        $this->assertArrayHasKey('title', $test);
        $this->assertArrayHasKey('dummy', $test);
        
        $project->save();
        
        // Check that unknown attributes still exists in the object after saving
        $test = $project->getAttributes();
        $this->assertArrayHasKey('title', $test);
        $this->assertArrayHasKey('dummy', $test);
    }

    /**
     * Test isAllowedTo
     *
     * @return void
     */
    public function testGetCRUD()
    {
        // Set Authenticated user
        $this->setAuthenticatedUser();

        $project = new Projects;
        $project->title = 'Some project';

        // Test new item
        $test = $project->getCRUD();
        $this->assertEquals($test, '1111');
        $test = $project->isAllowedTo('delete');
        $this->assertTrue($test);

        // Test updated item
        $project->save();
        $test = $project->getCRUD();
        $this->assertEquals($test, '1111');
        $test = $project->isAllowedTo('delete');
        $this->assertTrue($test);
    }

    /**
     * Test isAllowedTo (cRud)
     *
     * @return void
     */
    public function testSaveLowCrudCreation()
    {
        // Set Authenticated user
        $this->setAuthenticatedUser();

        $crud = new Cruds;
        $crud->title = 'Some title';
        $crud->setCrudOwner('0100');
        $crud->setCrud('0100');

        // Test created item
        $this->expectException(Exception::class);
        $crud->save();
    }

    /**
     * Test isAllowedTo (CRud)
     *
     * @return void
     */
    public function testSaveLowCrudUpdate()
    {
        // Set Authenticated user
        $this->setAuthenticatedUser();

        $crud = new Cruds;
        $crud->title = 'Some title';
        $crud->setCrudOwner('1100');
        $crud->setCrud('1100');

        $crud->save();
        $crud->title = 'Some new title';

        // Test updated item
        $this->expectException(Exception::class);
        $crud->save();
    }

    /**
     * Test Save
     *
     * @return void
     */
    public function testSaveNoDirty()
    {
        // Set Authenticated user
        $this->setAuthenticatedUser();

        $project = new Projects;
        $project->title = 'Some title';
        $project->save();
        // This should do nothing, just return a true
        $test = $project->save();
        $this->assertTrue($test);
    }

    /**
     * Test getAuthUserId
     *
     * @return void
     */
    public function testGetAuthUserId()
    {
        // Set Authenticated user
        $user = $this->setAuthenticatedUser();
        
        $project = new Projects;
        $test = $project->getAuthUserId();
        $this->assertEquals($test, $user->id);
    }

    /**
     * Test getAuthUserId Fail
     *
     * @return void
     */
    public function testGetAuthUserIdFail()
    {
        $project = new Projects;
        $test = $project->getAuthUserId();
        $this->assertEquals($test, 0);
    }

    /**
     * Test isAllowedTo (CRUd)
     *
     * @return void
     */
    public function testDeleteFail()
    {
        // Set Authenticated user
        $this->setAuthenticatedUser();

        $crud = new Cruds;
        $crud->title = 'Some title';
        $crud->setCrudOwner('1110');
        $crud->setCrud('1110');
        $crud->save();


        // Test updated item
        $this->expectException(Exception::class);
        $crud->delete();
    }

    /**
     * Test toJson
     *
     * @return void
     */
    public function testToJson()
    {
        // Set Authenticated user
        $this->setAuthenticatedUser();

        $project = new Projects;
        $project->title = 'Some title';
        $test = $project->toJson();
        // Check some values
        $this->assertEquals($test, '{"title":"Some title"}');
    }
}
