<?php

namespace Brunoocto\Vmodel\Tests\Unit\Models;

use Brunoocto\Vmodel\Tests\TestCase;
use Brunoocto\Vmodel\Models\VmodelUsers;

class VmodelUsersTest extends TestCase
{
    /**
     * Test
     *
     * @return void
     */
    public function testAllMethods()
    {
        $user = new VmodelUsers;

        $test = $user->save();
        $this->assertFalse($test);

        $test = $user->delete();
        $this->assertFalse($test);

        $test = $user->restore();
        $this->assertFalse($test);
    }
}
