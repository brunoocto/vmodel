<?php

namespace Brunoocto\Vmodel\Tests\Unit\Auth;

use Brunoocto\Vmodel\Tests\TestCase;
use Brunoocto\Vmodel\Models\VmodelUsers;
use Brunoocto\Sample\Models\ProjectManagement\Users;

class VmodelUserIdGuardTest extends TestCase
{
    /**
     * Test
     *
     * @return void
     */
    public function testGuardUsers()
    {
        // Set Authenticated user guard
        $this->app['config']->set('auth.defaults.guard', 'vmodel');
        $this->app['config']->set('auth.providers.vmodel.model', Users::class);

        request()->headers->set('user-id', null);

        $test = \Auth::id();
        $this->assertNull($test);

        // Instantiate at least one user considerate as Authenticated user
        $user = factory(Users::class)->create();
        
        // Simulate to have the header "User-Id"
        request()->headers->set('user-id', $user->id);

        $test = \Auth::check();
        $this->assertTrue($test);

        $test = \Auth::guest();
        $this->assertFalse($test);

        $test = \Auth::user();
        $this->assertInstanceOf(Users::class, $test);

        $test = \Auth::getHeaderUsersId();
        $this->assertEquals($test, 1);

        $test = \Auth::id();
        $this->assertEquals($test, 1);

        $test = \Auth::validate();
        $this->assertFalse($test);

        $user = new VmodelUsers;
        $test = \Auth::setUser($user);

        $this->assertTrue(true);
    }
}
