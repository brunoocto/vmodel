<?php

namespace Brunoocto\Vmodel\Tests\Unit\Auth;

use Brunoocto\Vmodel\Tests\TestCase;
use Brunoocto\Vmodel\Models\VmodelUsers;
use Brunoocto\Sample\Models\ProjectManagement\Users;
use Illuminate\Http\JsonResponse;

class VmodelUserProviderTest extends TestCase
{
    /**
     * Test retrieveById without User
     *
     * @return void
     */
    public function testRetrieveByIdWithoutUser()
    {
        // Set Authenticated user guard
        $this->app['config']->set('auth.defaults.guard', 'vmodel');
        $this->app['config']->set('auth.providers.vmodel.model', Users::class);
        
        // Get the Authentication provider
        $provider = \Auth::getProvider();

        // Test without User
        $test = $provider->retrieveById(1);
        // $test is an instance of Illuminate\Http\JsonResponse and with an Unauthorized status code
        $this->assertInstanceOf(JsonResponse::class, $test);
        $this->assertEquals($test->status(), 401);
    }

    /**
     * Test retrieveById with user
     *
     * @return void
     */
    public function testRetrieveByIdWithUser()
    {
        // Set Authenticated user guard
        $this->app['config']->set('auth.defaults.guard', 'vmodel');
        $this->app['config']->set('auth.providers.vmodel.model', Users::class);
        
        // Get the Authentication provider
        $provider = \Auth::getProvider();

        // Test with a User
        $user = factory(Users::class)->create();
        $test = $provider->retrieveById($user->id);
        $this->assertInstanceOf(Users::class, $test);
    }

    /**
     * Test retrieveById with virtual User
     *
     * @return void
     */
    public function testRetrieveByIdWithVirtualUser()
    {
        // Set Authenticated user guard
        $this->app['config']->set('auth.defaults.guard', 'vmodel');
        $this->app['config']->set('auth.providers.vmodel.model', VmodelUsers::class);
        
        // Get the Authentication provider
        $provider = \Auth::getProvider();

        // Test with a Virtual User
        $test = $provider->retrieveById(1);
        $this->assertInstanceOf(VmodelUsers::class, $test);
    }

    /**
     * Test retrieveById with fake User (no existance)
     *
     * @return void
     */
    public function testRetrieveByIdWithFakelUser()
    {
        // Set Authenticated user guard
        $this->app['config']->set('auth.defaults.guard', 'vmodel');
        $this->app['config']->set('auth.providers.vmodel.model', '\fake\User');
        //$user = \Auth::user();
        
        // Get the Authentication provider
        $provider = \Auth::getProvider();

        // Test with a fake User
        $test = $provider->retrieveById(1);
        // $test is an instance of Illuminate\Http\JsonResponse and with an Unauthorized status code
        $this->assertInstanceOf(JsonResponse::class, $test);
        $this->assertEquals($test->status(), 401);
    }

    /**
     * Test retrieveByToken
     *
     * @return void
     */
    public function testRetrieveByToken()
    {
        // Set Authenticated user guard
        $this->app['config']->set('auth.defaults.guard', 'vmodel');
        $this->app['config']->set('auth.providers.vmodel.model', VmodelUsers::class);
        
        // Get the Authentication provider
        $provider = \Auth::getProvider();

        // Test with a Virtual User
        $test = $provider->retrieveByToken(1, 1);
        $this->assertNull($test);
    }

    /**
     * Test updateRememberToken
     *
     * @return void
     */
    public function testUpdateRememberToken()
    {
        // Set Authenticated user guard
        $this->app['config']->set('auth.defaults.guard', 'vmodel');
        $this->app['config']->set('auth.providers.vmodel.model', VmodelUsers::class);
        
        // Get the Authentication provider
        $provider = \Auth::getProvider();

        // Get virtual user
        $user = new VmodelUsers;

        // Test with a Virtual User
        $provider->updateRememberToken($user, 1);
        // Since no return value, we just insure the code run
        $this->assertTrue(true);
    }

    /**
     * Test retrieveByToken
     *
     * @return void
     */
    public function testRetrieveByCredentials()
    {
        // Set Authenticated user guard
        $this->app['config']->set('auth.defaults.guard', 'vmodel');
        $this->app['config']->set('auth.providers.vmodel.model', VmodelUsers::class);
        
        // Get the Authentication provider
        $provider = \Auth::getProvider();

        // Test with a Virtual User
        $test = $provider->retrieveByCredentials([]);
        $this->assertNull($test);
    }

    /**
     * Test validateCredentials
     *
     * @return void
     */
    public function testValidateCredentials()
    {
        // Set Authenticated user guard
        $this->app['config']->set('auth.defaults.guard', 'vmodel');
        $this->app['config']->set('auth.providers.vmodel.model', VmodelUsers::class);
        
        // Get the Authentication provider
        $provider = \Auth::getProvider();

        // Get virtual user
        $user = new VmodelUsers;

        // Test with a Virtual User
        $provider->validateCredentials($user, []);
        // Since no return value, we just insure the code run
        $this->assertFalse(false);
    }
}
