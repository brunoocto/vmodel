<?php

namespace Brunoocto\Vmodel\Tests\Unit\Services;

use Brunoocto\Vmodel\Tests\TestCase;
use Brunoocto\Vmodel\Services\VmodelService;

class VmodelServiceTest extends TestCase
{
    /**
     * Test snakeToCamel
     *
     * @return void
     */
    public function testSnakeToCamel()
    {
        $string = 'some_test';
        $string = VmodelService::snakeToCamel($string);
        $this->assertEquals($string, 'someTest');
    }

    /**
     * Test camelToSnake
     *
     * @return void
     */
    public function testCamelToSnake()
    {
        $string = 'someTest';
        $string = VmodelService::camelToSnake($string);
        $this->assertEquals($string, 'some_test');
    }

    /**
     * Test snakeToPascal
     *
     * @return void
     */
    public function testSnakeToPascal()
    {
        $string = 'some_test';
        $string = VmodelService::snakeToPascal($string);
        $this->assertEquals($string, 'SomeTest');
    }
}
