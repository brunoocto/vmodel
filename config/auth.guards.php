<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Users Provider
    |--------------------------------------------------------------------------
    |
    | Register the Provider to identify the User by it's ID
    |
    */

    'vmodel' => [
        'driver' => 'user_id',
        'provider' => 'vmodel',
    ],

];
