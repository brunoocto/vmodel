<?php

use Brunoocto\Vmodel\Models\VmodelUsers;

return [

    /*
    |--------------------------------------------------------------------------
    | Users Provider
    |--------------------------------------------------------------------------
    |
    | Register the Provider to identify the User by it's ID
    |
    */

    'vmodel' => [
        'driver' => 'vmodel_auth',
        // VmodelUsers is a non-persistent Class
        'model' => VmodelUsers::class,
    ],

];
