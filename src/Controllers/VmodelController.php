<?php

namespace Brunoocto\Vmodel\Controllers;

use Brunoocto\Vmodel\Models\VmodelModel;
use CloudCreativity\LaravelJsonApi\Http\Controllers\JsonApiController;

/**
 * Class VmodelController
 * Extend JsonApiController to add restoration
 *
 */
class VmodelController extends JsonApiController
{
    /**
     * Restore resource action.
     *
     * @param VmodelModel $model
     * @return Response
     */
    public function restore(VmodelModel $model)
    {
        // Get the class name
        $class = get_class($model);
        // Check if it's asoft deleted class
        if ($class::isSoftDeletes()) {
            // Restore the item
            $model->restoreItem();
        }
        // Display the content
        return $this->reply()->content($model);
    }

    /**
     * Restore resource action.
     *
     * @param VmodelModel $model
     * @return Response
     */
    public function forceDelete(VmodelModel $model)
    {
        // Force to delete the item
        $model->forceDelete();
        return $this->reply()->deleted(null);
    }
}
