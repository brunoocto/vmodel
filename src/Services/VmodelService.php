<?php

namespace Brunoocto\Vmodel\Services;

use Brunoocto\Filesystem\Services\FolderService;
use Brunoocto\Vmodel\Models\VmodelModel;

/**
 * Vmodel service
 *
 */
class VmodelService
{
    /**
     * FolderService instance
     *
     * @var Brunoocto\Filesystem\Services\FolderService
     */
    protected $folder;

    /**
     * Convert snake_case to camelCase
     *
     * @param string $string
     * @return string
     */
    public static function snakeToCamel(string $string)
    {
        return str_replace(' ', '', (lcfirst(ucwords(str_replace('_', ' ', (strtolower($string)))))));
    }

    /**
     * Convert snake_case to camelCase
     *
     * @param string $string
     * @return string
     */
    public static function camelToSnake(string $string)
    {
        return str_replace(' ', '_', strtolower(preg_replace('/(\w)([A-Z])/', '$1 $2', $string)));
    }

    /**
     * Convert snake_case to PascalCase
     *
     * @param string $string
     * @return string
     */
    public static function snakeToPascal(string $string)
    {
        return str_replace(' ', '', (ucwords(str_replace('_', ' ', (strtolower($string))))));
    }

    /**
     * Constructor
     *
     * @param Brunoocto\Filesystem\Services\FolderService $folder
     * @return void
     */
    public function __construct(FolderService $folder)
    {
        $this->folder = $folder;
    }

    /**
     * Map routes for Vmodel resources as standard
     *
     * GET    /{type}
     * GET    /{type}/{id}
     * GET    /{type}/{id}/relationships/{relationship_type}
     * GET    /{type}/{id}/relationships/{relationship_type}/{relationship_id}
     * GET    /{type}/{id}/{relationship_type}
     * POST   /{type}
     * PATCH  /{type}/{id}
     * DELETE /{type}/{id}
     * PUT    /{type}/{id}/restore
     * PUT    /{type}/{id}/force_delete
     * POST   /{type}/{id}/relationships/{relationship_type}
     * PATCH  /{type}/{id}/relationships/{relationship_type}
     * PATCH  /{type}/{id}/relationships/{relationship_type}/{relationship_id}
     * DELETE /{type}/{id}/relationships/{relationship_type}
     * DELETE /{type}/{id}/relationships/{relationship_type}/{relationship_id}
     *
     * @example
     * \LinckoVmodel::mapConfigResources(__DIR__.'/../../config/json-api.resources.php');
     *
     * @param string $config_path Absolute or Relative path to the that contains all resources to load
     * @return void
     */
    public function mapConfigResources(string $config_path)
    {
        // Check if the config file is reachable
        if (is_file($config_path)) {
            // Load the config array as a variable.
            // array_intersect() helps to exclude any resource not previously loaded in ServiceProvider@register()
            $resources = array_intersect(include $config_path, config('json-api-brunoocto-vmodel.resources'));
            // Check if it's an array
            if (!empty($resources) && is_array($resources)) {

                // Add routes GET, POST, PATCH, DELETE
                \JsonApi::register('brunoocto-vmodel')
                    ->routes(function ($api, $router) use ($resources) {
                        // For each rource, we map VND routes
                        foreach ($resources as $type => $class) {
                            // Map the resource with POST, GET, PATCH, DELETE
                            $vnd = $api->resource($type);
                            // Make a condition on ID, must be numerical
                            $vnd->id('[\d]+');
                            // Get all relationships
                            $relationships = $class::getModelRelationships();
                            foreach ($relationships as $method => $relation) {
                                // MAP relationships
                                $vnd->relationships(function ($relations) use ($method, $relation) {
                                    // Get the relation type, hasOne or hasMany
                                    $relation_type = VmodelModel::getRelationType($relation[3]);
                                    // Enable only the redeability via relationships' link
                                    if ($relation_type == 'hasMany') {
                                        $relations->hasMany($method)->readOnly();
                                    } elseif ($relation_type == 'hasOne') {
                                        $relations->hasOne($method)->readOnly();
                                    }
                                });
                            }
                            // Add more routes only for SoftDeletes items
                            if ($class::isSoftDeletes()) {
                                $api->resource($type)->controller('\Brunoocto\Vmodel\Controllers\VmodelController')->routes(function ($model) use ($type) {
                                    // Add restoration capability
                                    $model->put('{record}/restore', 'restore')->name('restore');
                                    // Add force delettion
                                    $model->put('{record}/force_delete', 'forceDelete')->name('force_delete');
                                });
                            }
                        }
                    });
            }
        }
    }
}
