<?php

namespace Brunoocto\Vmodel\Auth;

use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Auth\Authenticatable;
use Brunoocto\Vmodel\Models\VmodelUsers;

class VmodelUserProvider implements UserProvider
{
    /**
     * Model Class to identify the user
     *
     * @var null|integer
     */
    protected $users_model = null;

    /**
     * User ID identified.
     *
     * @var null|integer
     */
    protected $users_id = null;

    /**
     * Constructor
     * Vmodel is made for API resources, which are not secured for speed optimization since internal to our server architecture. "users_id" are thus send as header.
     *
     * @return void
     */
    public function __construct($users_model)
    {
        // Check if the Users Model exists
        if (class_exists($users_model)) {
            $this->users_model = $users_model;
            return true;
        }

        // Inform the client that information to identify the user are not enough
        return \LinckoJson::error(401, 'The system was not able to identify the user.');
    }

    /**
     * Retrieve a user by their unique identifier.
     *
     * @param  mixed  $identifier
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveById($identifier)
    {
        // If Virtual User
        if ($this->users_model == VmodelUsers::class) {
            // If we use Vmodel default Virtual Users, it is loaded in memory only, it's a non-persistent object
            $user = new VmodelUsers();
            $user->id = $identifier;
            return  $user;
        }
        // Otherwise return any existing User Model
        if ($this->users_model && $user = $this->users_model::find($identifier)) {
            return  $user;
        }
        return \LinckoJson::error(401, 'The system was not able to identify the user.');
    }

    /**
     * Retrieve a user by their unique identifier and "remember me" token.
     *
     * @param  mixed  $identifier
     * @param  string  $token
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByToken($identifier, $token)
    {
        return null;
    }

    /**
     * Update the "remember me" token for the given user in storage.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @param  string  $token
     * @return void
     */
    public function updateRememberToken(Authenticatable $user, $token)
    {
        // Do nothing
    }

    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array  $credentials
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByCredentials(array $credentials)
    {
        return null;
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @param  array  $credentials
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        return false;
    }
}
