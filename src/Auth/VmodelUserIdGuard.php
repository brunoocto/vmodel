<?php

namespace Brunoocto\Vmodel\Auth;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Auth\Authenticatable;
use CloudCreativity\LaravelJsonApi\Routing\Route;

class VmodelUserIdGuard implements Guard
{
    protected $request;
    protected $provider;
    protected $user;

    /**
     * Create a new authentication guard.
     *
     * @param  \Illuminate\Contracts\Auth\UserProvider  $provider
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function __construct(UserProvider $provider, Request $request)
    {
        $this->request = $request;
        $this->provider = $provider;
        $this->user = null;
    }

    /**
     * Determine if the current user is authenticated.
     *
     * @return bool
     */
    public function check()
    {
        return ! is_null($this->user(false));
    }

    /**
     * Determine if the current user is a guest.
     *
     * @return bool
     */
    public function guest()
    {
        return ! $this->check();
    }

    /**
     * Get the currently authenticated user.
     *
     * @param bool $alert Send Error message
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function user($alert = true)
    {
        if (is_null($this->user)) {
            $user_id = $this->getHeaderUsersId();
            if (is_numeric($user_id) && $user_id > 0) {
                // If the user is not set yet, we retreive it
                $this->user = $this->provider->retrieveById($this->getHeaderUsersId());
            }
        }
        if (is_null($this->user) && $alert) {
            // Inform the client that information to identify the user are not enough
            \LinckoJson::error(401, 'The system was not able to identify the user.');
        }
        return $this->user;
    }

    /**
     * Get the header 'User-Id params from the current request
     *
     * @return string
     */
    public function getHeaderUsersId()
    {
        // Get the user ID by it's header
        return request()->header('user_id');
    }

    /**
     * Get the ID for the currently authenticated user.
     *
     * @return string|null
    */
    public function id()
    {
        // Get HTTP Method
        $method = mb_strtolower(request()->getMethod());

        // "app()->make(Route::class)->getType()" Get the Object we are requesting, if we want to create a Users, we skip it and return null as ID
        if ($method == 'post' && app()->make(Route::class)->getType() == config('auth.providers.vmodel.model')) {
            return null;
        // For any other request, we check if the user is authentcated
        } elseif ($user = $this->user()) {
            return $user->id;
        }
        return null;
    }

    /**
     * Validate a user's credentials.
     *
     * @return bool
     */
    public function validate(array $credentials=[])
    {
        return false;
    }

    /**
     * Set the current user.
     *
     * @param  Array $user User info
     * @return void
     */
    public function setUser(Authenticatable $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Get the Provider
     *
     * @return VmodelUserProvider
     */
    public function getProvider()
    {
        return $this->provider;
    }
}
