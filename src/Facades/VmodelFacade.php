<?php

namespace Brunoocto\Vmodel\Facades;

use Illuminate\Support\Facades\Facade;

class VmodelFacade extends Facade
{
    /**
    * Get the registered name of the component.
    *
    * @return string
    */
    protected static function getFacadeAccessor()
    {
        return 'lincko_vmodel';
    }
}
