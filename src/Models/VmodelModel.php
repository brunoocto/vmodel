<?php

namespace Brunoocto\Vmodel\Models;

use Exception;
use \Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use CloudCreativity\LaravelJsonApi\Rules\HasMany;
use CloudCreativity\LaravelJsonApi\Rules\HasOne;

/**
 * Handle Vmodel operations
 * Note: This class cannot be used as abstract because dependency injection wont work.
 *
 */
class VmodelModel extends Model
{
    /**
     * The class name of the parent model.
     *
     * @var string
     */
    protected $morphClass;

    /**
     * The attributes that should be cast to native types.
     * Note that Laravel is using Carbon, so timestamp can be returned in milliseconds.
     * By default these keys are added to $visible for response output format, they will be ignored if they don't exisit in the DB's table.
     *
     * @var array
     */
    protected static $generic_casts = [
        // For date we use timestamp to make sure there is no timezone confusion. DB data are store in UTC Date format for better readability only.
        'id'         => 'string',
        'created_at' => 'datetime:U.u',
        'updated_at' => 'datetime:U.u',
        'deleted_at' => 'datetime:U.u',
        'created_by' => 'string',
        'updated_by' => 'string',
        'deleted_by' => 'string',
    ];

    /**
     * Map Eloquent relation method hasOne or hasMany type
     *
     * @var array
     */
    protected static $map_relationships = [
        'hasOne' => [
            'hasOne',
            'hasOneThrough',
            'belongsTo',
            'morphTo',
        ],
        'hasMany' => [
            'hasMany',
            'belongsToMany',
            'hasManyThrough',
            'morphMany',
            'morphToMany',
            'morphedByMany',
        ],
    ];

    /**
    * Relationships
    * The format must be:
    * [
    *     ...
    *     {display name} => [ // Name display on front device
    *         {authorize input}, // Boolean, at True we authorize the client to add/update the relationship
    *         {public}, // Boolean value, at true it will make it available in front
    *         {method name}, // NOTE: It can be different as the type name, get the type from the class name instead
    *         {relation type}, // Eloquent relationship method name (hasMany, morphTo, etc.)
    *         {class name},
    *     ]
    *     'tasks' => [
    *         true,
    *         true,
    *         'tasks',
    *         'hasMany',
    *         Tasks::class,
    *     ]
    *     ...
    * ]
    *
    * @var array
    */
    protected static $model_relationships = [];

    /**
     * Keep a record of all existing columns in the corresponding database's table
     *
     * @var array
     */
    protected static $columns = [];

    /**
     * Storage validation rules
     * https://laravel-json-api.readthedocs.io/en/stable/basics/validators/
     *
     * @var array
     */
    protected static $rules = [];

    /**
     * The attributes that aren't mass assignable.
     * By default we allow to fill all attributes.
     * Set $fillable or $guarded in the Model itself if you want to specify any limitation.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * List visible keys for json response.
     * Anything starting with the prefix "_(up|down)_(one|many)_" is reserved for relationships.
     * VmodelModel does always add by default (via self::$generic_casts) the following fields:
     *  'id',
     *  'created_at',
     *  'created_by',
     *  'updated_at',
     *  'updated_by',
     *  'deleted_at',
     *  'deleted_by',
     *
     * @var array
     */
    protected $visible = [];

    /**
     * All of the relationships to be touched.
     * It does update the column "updated_at" if it exists
     * https://laravel.com/docs/6.x/eloquent-relationships#touching-parent-timestamps
     *
     * @var array
     */
    protected $touches = [];


    /**
     * Default binary CRUD restriction per model class
     * 0100:  R
     * 1100: CR
     * 1110: CRU
     * 1111: CRUD (default)
     *
     *  @var array
     */
    protected $crud = '1111';

    /**
     * Default binary CRUD restriction per model class for the owner
     * 0100:  R
     * 1100: CR
     * 1110: CRU
     * 1111: CRUD (default)
     *
     *  @var array
     */
    protected $crud_owner = '1111';

    /**
     * Default primary key
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Get the relation type, hasOne or hasMany
     *
     * @return string|null
     */
    public static function getRelationType($relation)
    {
        // Scan all mapped Eloquent relation methods' name
        foreach (static::$map_relationships as $key => $value) {
            // Return the type of relation if it's mapped
            if (in_array($relation, static::$map_relationships[$key])) {
                return $key;
            }
        }
        return null;
    }

    /**
     * Check if the Model is a SoftDeletes version
     *
     * @return boolen
     */
    public static function isSoftDeletes()
    {
        return in_array(SoftDeletes::class, class_uses(get_called_class()));
    }

    /**
     * Return the table columns list of the object
     * TODO (cache) => Since getSchemaBuilder is oftenly used and won't change, we can use Redis. Just make sure Redis is cleared after a version update.: '1.0.24_{table_name}_getSchemaBuilder' => {value}
     *
     * @return array
     */
    public static function getColumns()
    {
        // Get an instance of Model class
        $model = new static();
        // Check if it has been cached
        if (!isset(static::$columns[$model->getTable()])) {
            // Initialize
            static::$columns[$model->getTable()] = [];
            // Get the builder of Model connection
            $schema = \DB::connection($model->getConnectionName())->getSchemaBuilder();
            // Store the column list
            static::$columns[$model->getTable()] = $schema->getColumnListing($model->getTable());
        }
        return static::$columns[$model->getTable()];
    }

    /**
     * Return the relationship list
     * TODO (cache) => this should be cacheable since it should not change (expect after a version update). We can use App version as prefix (with expiration to 24H for instance): '1.0.24_{table_name}_getAuthorizedModelRelationships' => {value}
     *
     * @return array
     */
    public static function getAuthorizedModelRelationships()
    {
        // Filter the relationships array by keeping only authorized ones
        $authorized_model_relationships = array_filter(static::$model_relationships, function ($value) {
            // The second key at true tells that the relation is authorized to be manipulated by the client
            return boolval($value[0]);
        });

        return $authorized_model_relationships;
    }

    /**
     * Return the relationship list
     * TODO (cache) => this should be cacheable since it should not change (expect after a version update). We can use App version as prefix (with expiration to 24H for instance): '1.0.24_{table_name}_getPublicModelRelationships' => {value}
     *
     * @return array
     */
    public static function getPublicModelRelationships()
    {
        // Filter the relationships array by keeping only visible ones
        $public_model_relationships = array_filter(static::$model_relationships, function ($value) {
            // The second key at true tells that the relation is visible
            return boolval($value[1]);
        });

        return $public_model_relationships;
    }

    /**
     * Return all relationships
     *
     * @return array
     */
    public static function getModelRelationships()
    {
        return static::$model_relationships;
    }

    /**
     * Get Model relationship for a specific field
     *
     * @param string $field Display name of the relationship
     * @return array|null
     */
    public static function getModelRelationship(string $field)
    {
        // (editable and non-editable) We allow relationships to be displayed (exemple: tasks/relationships/_projects)
        if (mb_strtolower(request()->getMethod()) == 'get') {
            return static::$model_relationships[$field];
        // (editable only) We restrict for all other methods (POST and PATCH, that contains "relationships" to be modified)
        } elseif (isset(static::$model_relationships[$field]) && static::$model_relationships[$field][0]) {
            return static::$model_relationships[$field];
        }
        return null;
    }

    /**
     * Return the rules
     *
     * @return array
     */
    public static function getRulesList()
    {
        return static::$rules;
    }

    /**
     * Uncast value
     * IMPORTANT: Do not overwrite this method in children, we may lose the uncast of generic_cast (the dates)
     *
     * @param string $field The name of the field we want to uncast
     *
     * @return mix
     */
    public static function uncast(string $field, $value)
    {
        if (in_array($field, ['created_at', 'updated_at', 'deleted_at'])) {
            $value = 1000*floatval($value);
            // Use the database format "datetime" in milliseconds
            // return Carbon::createFromTimestampMs($value)->format('Y-m-d H:i:s.u');
            // NOTE: I deleted microseconds ".u" because SQLite does not recognize it while comparing dates
            return Carbon::createFromTimestampMs($value)->format('Y-m-d H:i:s');
        }
        // Do nothing
        return $value;
    }

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        // We merge generic cast (data convertion)
        $this->casts = array_merge(static::$generic_casts, $this->casts);

        // We merge key only from generic cast (data convertion) to make them visible in output
        $this->visible = array_merge(array_keys(static::$generic_casts), $this->visible);

        // If the variable is not set in the model we get it from the table name
        if (empty($this->morphClass)) {
            // We set Polymorphic variable
            $this->morphClass = $this->table;
        };

        parent::__construct($attributes);
    }

    /**
     * Get the name for polymorphic relations.
     * We overwrite this function because Eloquent return a Class name but I prefer to store table name instead
     *
     * @return string
     */
    public function getMorphClass()
    {
        $morphClass = parent::getMorphClass();
        // If $name is a Model class
        if (is_subclass_of($morphClass, VmodelModel::class)) {
            // We get the table name
            $morphClass = (new $morphClass)->getTable();
        }
        return $morphClass;
    }

    /**
     * Return the validator rules
     *
     * @return array
     */
    public function getRules()
    {
        // Initialize $rules
        $rules = [];

        // We get the json body sent
        $content = json_decode(request()->getContent());

        // Get HTTP Method
        $method = mb_strtolower(request()->getMethod());

        // List of authorized relationships
        $authorized_model_relationships = static::getAuthorizedModelRelationships();

        if ($method == 'post') {
            // Get all rules at creation
            $rules = static::$rules;
        } elseif (isset($content->data) && isset($content->data->attributes)) {
            // We just check all attributes sent
            foreach ($content->data->attributes as $key => $value) {
                if (isset(static::$rules[$key])) {
                    // Add the existing attributes key to the rules to only check them
                    $rules[$key] = static::$rules[$key];
                }
            }
        }

        // If relationships is required, we check that one exist at least
        if ($method == 'post' && isset($rules['relationships']) && $rules['relationships'] == 'required') {
            $pass = false;
            if (isset($content->data) && isset($content->data->relationships)) {
                foreach ($content->data->relationships as $display_name => $value) {
                    if (isset($authorized_model_relationships[$display_name])) {
                        // At least one relationship found
                        $pass = true;

                        //Get the relation detail
                        $relation = $authorized_model_relationships[$display_name];
                        // Get the relation type, hasOne or hasMany
                        $relation_type = static::getRelationType($relation[3]);

                        // Default to hasOne (because it is the most common one)
                        $class = HasOne::class;
                        if ($relation_type == 'hasMany') {
                            // Switch to hasMany if it's a collection
                            $class = HasMany::class;
                        }
                        // Setup a required field
                        $rules[$display_name] = [
                            'required',
                            // It will be for instance: new hasOne('projects') , 'projects' is the display_name here
                            new $class((new $relation[4])->getTable()),
                        ];
                    }
                }
            }
            if (!$pass) {
                throw new Exception('A relationship is required.', 400);
            }
        }

        // Non-polomorphic (required-only)
        // Check first the relationship key as key value 'projects' => 'required', which works only for non-polymorphic relationships
        // Get all public relationships
        foreach ($authorized_model_relationships as $display_name => $relation) {
            if (isset($rules[$display_name]) && !is_string($rules[$display_name])) {
                // If the rules is an object or an array, it means that we already treated the rule properly, so we can safely skip
                continue;
            }
            // Get the relation type, hasOne or hasMany
            $relation_type = static::getRelationType($relation[3]);

            // Default to hasOne (because it is the most common one)
            $class = HasOne::class;
            if ($relation_type == 'hasMany') {
                // Switch to hasMany if it's a collection
                $class = HasMany::class;
            }

            // If we use the relation name 'projects' => 'required'
            if (isset($rules[$display_name]) && $rules[$display_name] == 'required') {
                // Setup a required field
                $rules[$display_name] = [
                    'required',
                    // It will be for instance: new hasOne('projects') , 'projects' is the method_name here
                    new $class((new $relation[4])->getTable()),
                ];
            } elseif (!isset($rules[$display_name])) {
                // For a non-required field
                $rules[$display_name] = new $class((new $relation[4])->getTable());
            }
        }

        // 'relationships' is used as a generic key, so make sure to remove the key to not generate a error message
        unset($rules['relationships']);

        return $rules;
    }

    /**
     * Get the attributes that have been changed since last sync.
     * Make sure that we only work with existing columns in the database
     *
     * @return array
     */
    public function getDirty()
    {
        // Get all changes
        $dirty = parent::getDirty();
        // Get table's columns
        $columns = static::getColumns();
        foreach ($dirty as $key => $value) {
            if (!in_array($key, $columns)) {
                // Do not keep any dirty value that does not actually exist in the database
                unset($dirty[$key]);
            }
        }
        return $dirty;
    }

    /**
     * Return the User-ID found via authentication middleware
     *
     * @return string
     */
    public function getAuthUserId()
    {
        if (\Auth::guard('vmodel')->guest()) {
            // Return 0 as Admin ID if no User is authenticated. The Authentication should be checked by a Middleware like 'api:auth' or 'auth' previously. It is not this class responsibility.
            return 0;
        }
        return \Auth::guard('vmodel')->id();
    }

    /**
     * Get the item CRUD allowed for the current user
     *
     * @return string binray CRUD representation
     */
    public function getCRUD()
    {
        // Default to Generic
        $crud = $this->crud;

        // Get User ID
        $user_id = $this->getAuthUserId();

        // Get all database existing fields
        $columns = static::getColumns();

        if (in_array('created_by', $columns) && (!isset($this->id) || $this->created_by == $user_id)) {
            // If new or if the user own the item
            $crud = $this->crud_owner;
        }

        return $crud;
    }

    /**
     * Tell if the User is allowed to make a CRUD operation
     *
     * @param string $action can be: 'create', 'read', 'update'|'edit', 'delete'|'remove'
     * @return boolean
     */
    public function isAllowedTo(string $action)
    {
        // Lower the case
        $action = strtolower($action);

        // Get the CRUD allowed
        $crud = $this->getCRUD();

        // Compare with the CRUD string (can be used as an Array)
        if ($action == 'create') {
            return isset($crud[0]) && boolval($crud[0]);
        } elseif ($action == 'read') {
            return isset($crud[1]) && boolval($crud[1]);
        } elseif ($action == 'update' || $action == 'edit') {
            return isset($crud[2]) && boolval($crud[2]);
        } elseif ($action == 'delete' || $action == 'remove' || $action == 'restore') {
            return isset($crud[3]) && boolval($crud[3]);
        }

        // By default we do not give the permission
        return false;
    }

    /**
     * Overwrite the method of touching the owning relations of the model.
     * And stop the propagation (unlike the orignal one from Eloquent) to avoid nested updates (can lead to infinite loop)
     *
     * @return void
     */
    public function touchOwners()
    {
        foreach ($this->touches as $relation) {
            $this->$relation()->touch();
        }
        // Do not propagate
    }

    /**
     * Save the model into the database.
     * It prepares some additional information like who did the save
     *
     * @return boolean
     */
    public function save(array $options = [])
    {
        // Build the list of parent models to touch
        $this->buildTouches();

        // Get all database existing fields
        $columns = static::getColumns();

        // Get all modified fields
        $dirty = $this->getDirty();

        // Define if it's a new item
        $new = !isset($this->id);
        // Get User ID
        $user_id = $this->getAuthUserId();

        // Check CRUD allowance
        if ($new && !$this->isAllowedTo('create')) {
            // Check if the model allows creation
            throw new Exception('Creation operation not allowed ['.$this->table.']', 405);
        } elseif (!$new && !$this->isAllowedTo('update')) {
            // Check if the model allows updating
            throw new Exception('Updating operation not allowed ['.$this->table.':'.$this->id.']', 405);
        }

        // For a new item, add who created it
        if ($new && in_array('created_by', $columns)) {
            $this->created_by = $user_id;
        }

        // For any change, add who updated it
        if (in_array('updated_by', $columns)) {
            $this->updated_by = $user_id;
        }

        // Insure to not send to the database any column that does not exists
        // Store those values to reapply them to the model after the saving operation
        $attributes = [];
        foreach ($this->attributes as $key => $value) {
            // Check if it's an existing column
            if (!in_array($key, $columns)) {
                $attributes[$key] = $this->attributes[$key];
                unset($this->attributes[$key]);
            }
        }

        // Do nothing if dirty is empty (idempotent => return the same object without touching it)
        if (count($dirty)<=0) {
            return true;
        }

        // JSON validation is done by ValidatedRequest while saving
        $return = parent::save($options);

        // Make sure that we return all DB table fields with the model while create an object (otherwise optional fields may not appear in the response)
        if ($new) {
            // We refresh only attributes (do not use refresh here, it bugs with morpthTo relation, the function load() in refresh() does not work with morphTo)
            $this->setRawAttributes(
                static::newQueryWithoutScopes()->findOrFail($this->getKey())->attributes
            );
        }

        // Reapply the columns that were not part of table columns
        foreach ($attributes as $key => $value) {
            $this->attributes[$key] = $value;
        }

        return $return;
    }

    /**
     * Delete the model
     * A successful deletion needs to return a status "204 No-Content"
     *
     * @return boolean
     */
    public function delete()
    {
        // Get all database existing fields
        $columns = static::getColumns();

        // Get User ID
        $user_id = $this->getAuthUserId();
        if (in_array('deleted_at', $columns)) {

            // Check if the model allows deletion
            // If the owner can be defined, we check if he is the owner
            if (!$this->isAllowedTo('delete')) {
                throw new Exception('Deletion operation not allowed ['.$this->table.':'.$this->id.']', 405);
            }

            // If the column exists, it keep track of who did the deletion
            if (in_array('deleted_by', $columns)) {
                $this->deleted_by = $user_id;
                $this->updated_by = $user_id;
                // We to to do a ->save() because ->delete() does not take into account any other column modification (like deleted_by)
                $this->save();
            }

            parent::delete();
        }

        return true;
    }

    /**
     * Restore the model
     * The method name must be different as restore() because it's been used by the trait SoftDeletes
     *
     * @return boolean
     */
    public function restoreItem()
    {
        // Store the success response
        // By default we return true to return item in the response
        $result = true;

        // Get all database existing fields
        $columns = static::getColumns();

        // Get User ID
        $user_id = $this->getAuthUserId();

        // Check if the model is
        if (in_array('deleted_at', $columns) && !is_null($this->deleted_at)) {

            // Check if the model allows restoration
            // If the owner can be defined, we check if he is the owner
            if (!$this->isAllowedTo('restore')) {
                throw new Exception('Restoration operation not allowed ['.$this->table.':'.$this->id.']', 405);
            }

            // If the column exists, it keep track of who did the restoration
            if (in_array('deleted_by', $columns)) {
                $this->deleted_by = null;
                $this->updated_by = $user_id;
            }

            $result = $this->restore();
        }

        return $result;
    }

    /**
     * We enable item restoration without filter information in URL
     * We use "/projects/13/restore" instead of "/projects/13/restore?filter[with-trashed]=true" which can easily lead to bug while developing
     *
     * @return Builder
     */
    public function newQuery()
    {
        // Get query object
        $query = parent::newQuery();
        // Check if it's a SofDelete model and if the route name is a restoration or force delete
        if (static::isSoftDeletes() && preg_match('/.*'.$this->getTable().'.(restore|force_delete)/', \Route::currentRouteName())) {
            // Authorize the gather the trashed (deleted_at not null) item
            $query = $query->withTrashed();
        }

        // In case later we want to use filter in query specifically for a Model, here is how to get the list of parameters (I leave it here because it took me days to find it without documentation)
        /*
        $parameters = app()->make(\Neomerx\JsonApi\Contracts\Encoder\Parameters\EncodingParametersInterface::class);
        $filters = collect($parameters->getFilteringParameters());
        */

        return $query;
    }

    /**
     * Check if the method requested is a relation method.
     *
     * @return boolean
     */
    public static function isRelationMethod($field)
    {
        // We check all relationships available
        foreach (static::$model_relationships as $relationships) {
            // We check if the method exists in the model
            if ($relationships[2] == $field && method_exists(get_called_class(), $field)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Overload morphTo to fix the issue that regardless the method name, the class is always recovered from other_type, which lead to multiple wrong relationships displayed.
     *
     * @param  string|null  $name We can pass a class here
     * @param  string|null  $type
     * @param  string|null  $id
     * @param  string|null  $ownerKey
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function morphTo($name = null, $type = null, $id = null, $ownerKey = null)
    {
        // If no name is provided, we will use the backtrace to get the function name
        // since that is most likely the name of the polymorphic interface. We can
        // use that to get both the class and foreign key that will be utilized.
        $name = $name ?: $this->guessBelongsToRelation();

        // Initialize the class
        $class = null;

        // If $name is a Model class
        if (is_subclass_of($name, VmodelModel::class)) {
            // Set the class if it is set
            $class = $name;
            // We get the table name
            $name = (new $name)->getTable();
        }

        // Set $type and $id if they are null
        [$type, $id] = $this->getMorphs(
            Str::snake($name),
            $type,
            $id
        );

        // Get the method name
        // Set same as the table name in camelCase (which is the standard format of methods)
        $method = Str::camel($name);

        // Check throw all relationships and get the proper method name
        foreach (static::$model_relationships as $relationships) {
            // Skip if not a morphTo
            if ($relationships[3] != 'morphTo') {
                continue;
            }
            // If the class match
            if ($class) {
                // We check that are using the same class
                if ($class == $relationships[4]) {
                    // If we we gets the method
                    $method = $relationships[2];
                    break;
                }
            } else {
                // Otherwise we check that the table name is matching
                $table = (new $relationships[4])->getTable();
                if ($name == $table) {
                    // If we we gets the method
                    $method = $relationships[2];
                    break;
                }
            }
        }

        // If we are not getting the same type stored as the relation type, we force to get an empty collection
        if ($name !== $this->{$type}) {
            // We fake the id of current instance to insure we cannot match any item.
            // The issue found was that the response returned projects ID 2 with tasks ID 2, while the stored instance is only tasks ID 2. The type was messing up.
            $id_stored = $this->{$id};
            // Give an ID that cannot exists
            $this->{$id} = -1;
            $instance = parent::morphTo($method, $type, $id, $ownerKey);
            // Return to the stored id
            $this->{$id} = $id_stored;
            // return an empty instance collection
            return $instance;
        }

        return parent::morphTo($method, $type, $id, $ownerKey);
    }

    /**
     * Convert the model instance to JSON.
     * Insure that the encoding is JSON_UNESCAPED_UNICODE
     *
     * @param  int  $options
     * @return string
     *
     * @throws \Illuminate\Database\Eloquent\JsonEncodingException
     */
    public function toJson($options = 0)
    {
        // Check the User ID is present
        $this->getAuthUserId();

        // 256: JSON_UNESCAPED_UNICODE
        return parent::toJson(256);
    }

    /**
     * Convert the model instance to an array.
     *
     * @return array
     */
    public function toArray()
    {
        // Check the User ID is present
        $this->getAuthUserId();

        return parent::toArray();
    }

    /**
     * Build the touches property to update parent timestamp "updated_at"
     *
     * @return voir
     */
    protected function buildTouches()
    {
        // We don't overwrite if it has been declared (not recommanded) in the Model
        if (empty($this->touches)) {
            // Check throw all relationships and get the proper method name
            foreach (static::$model_relationships as $model_relationship) {
                // From parent, get only the visible relation of current Model
                foreach ($model_relationship[4]::getPublicModelRelationships() as $parent_relationship) {
                    // If it concerns the current model
                    if (static::class && $parent_relationship[1] && !in_array($model_relationship[2], $this->touches)) {
                        // We store the relationship's method
                        $this->touches[] = $model_relationship[2];
                    }
                }
            }
        }
    }
}
