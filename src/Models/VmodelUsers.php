<?php

namespace Brunoocto\Vmodel\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * This class exists to virtualy create a user.
 * To use another Users Model (for instance linked to the database with specific users' fields) define it into your ServiceProvider in the boot() method.
 * config(['auth.providers.vmodel.model' => Namespace\To\MyUsersModel::class]);
 *
 * Here are links thta helped to setup Authentication:
 * https://laravel.com/docs/6.x/authentication#adding-custom-user-providers
 * https://code.tutsplus.com/tutorials/how-to-create-a-custom-authentication-guard-in-laravel--cms-29667
 *
 */
class VmodelUsers extends Authenticatable
{
    /**
     * Disable Creation/Updating
     *
     * @return false
     */
    public function save(array $options = [])
    {
        return false;
    }

    /**
     * Disable Deletion
     *
     * @return false
     */
    public function delete()
    {
        return false;
    }

    /**
     * Disable Restoration
     *
     * @return false
     */
    public function restore()
    {
        return false;
    }
}
