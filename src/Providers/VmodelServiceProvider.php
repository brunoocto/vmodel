<?php

namespace Brunoocto\Vmodel\Providers;

use Dotenv\Dotenv;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;
use Brunoocto\Vmodel\Services\VmodelService;
use Brunoocto\Vmodel\Models\VmodelModel;
use Brunoocto\Vmodel\Auth\VmodelUserProvider;
use Brunoocto\Vmodel\Auth\VmodelUserIdGuard;

class VmodelServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Load environment variables specific to the library (default is .env)
        $dotenv = Dotenv::createMutable(__DIR__.'/../../');
        $dotenv->load();
        
        // Merge configuration file
        $this->mergeConfigFrom(
            __DIR__.'/../../config/json-api.php',
            'json-api-brunoocto-vmodel'
        );

        // Add the Provider for users_id header authentication method
        $this->mergeConfigFrom(
            __DIR__.'/../../config/auth.providers.php',
            'auth.providers'
        );

        // Add the Guards for users_id header authentication method
        $this->mergeConfigFrom(
            __DIR__.'/../../config/auth.guards.php',
            'auth.guards'
        );

        // VmodelService binding
        // The alias make sure that the Facade (\LinckoVmodel::) and the Maker will work
        $this->app->alias(VmodelService::class, 'lincko_vmodel');
        // The singleton (or bind) set any specification at instanciation
        $this->app->singleton(VmodelService::class, VmodelService::class);

        // The singleton (or bind) set any specification at instanciation
        $this->app->bind(VmodelModel::class, VmodelModel::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Load the authentication method via header only.
        // https://laravel.com/docs/6.x/authentication#closure-request-guards
        \Auth::provider('vmodel_auth', function ($app, array $config) {
            return new VmodelUserProvider($config['model']);
        });

        // Add custom guard
        \Auth::extend('user_id', function ($app, $name, array $config) {
            return new VmodelUserIdGuard(\Auth::createUserProvider($config['provider']), $app->make('request'));
        });

        // We map morph relations
        Relation::morphMap(config('json-api-brunoocto-vmodel.resources'));

        // Add relationships resources
        $this->addRelationshipsResources();
    }

    /**
     * List all display_name of relationships so we can find there class
     * TODO (cache) => Since addRelationshipsResources is oftenly used and won't change, we can use Redis. Just make sure Redis is cleared after a version update.: '1.0.24_{table_name}_addRelationshipsResources' => {value}
     *
     * @return void
     */
    protected function addRelationshipsResources()
    {
        // We get all resources
        $resources = config('json-api-brunoocto-vmodel.resources');
        foreach ($resources as $class) {
            $relationships = $class::getModelRelationships();
            foreach ($relationships as $display_name => $relation) {
                // Check if the relationships name does not exist yet
                if (!array_key_exists($display_name, $resources)) {
                    // Add to the configuration to enable inverteType
                    config(['json-api-brunoocto-vmodel.resources.'.$display_name => $relation[4]]);
                }
            }
        }
    }
}
