<?php

namespace Brunoocto\Vmodel\JsonApi;

use CloudCreativity\LaravelJsonApi\Resolver\AbstractResolver;
use Brunoocto\Vmodel\JsonApi\VmodelAdapter;
use Exception;

// https://laravel-json-api.readthedocs.io/en/latest/features/resolvers/
class VmodelResolver extends AbstractResolver
{
    /**
     * Return the class path where the following class are accessible:
     *  - Adapter
     *  - Schema
     *  - Validators
     *
     * @param string $unit
     * @param $resource_type
     * @return string
     */
    protected function resolve($unit, $resource_type)
    {
        // Get the resource class, The namespace must be the same, and the naming append the unit
        $resource_class = config('json-api-brunoocto-vmodel.resources.'.$resource_type);
        
        // Check first if we have a specific Class per model.
        if (class_exists($resource_class.$unit)) {
            if (!is_subclass_of($resource_class.$unit, 'Brunoocto\Vmodel\JsonApi\Vmodel'.$unit)) {
                throw new Exception('The class '.$resource_class.$unit.' must extends of Brunoocto\Vmodel\JsonApi\Vmodel'.$unit, 501);
            }
            // If the calss exists and extend from Vmodel one, we use it
            return $resource_class.$unit;
        }
        // We need to adpat Adapter and Validators per model type
        if (in_array($unit, ['Adapter', 'Validators'])) {
            // We set a variable instead of using a parameter because we don't have the hand of the method called in the third library. It's unusual, but it still work
            VmodelAdapter::setModelClassConstructor($resource_class);
            // For Adpapter, we load sofdelete version or not depending of the type of class.
            // Check if the class exists and is a SoftDeletes version
            if ($unit == 'Adapter' && class_exists($resource_class) && $resource_class::isSoftDeletes()) {
                // We load a specific Adapter version that support SoftDeletes (this class extends from VmodelAdapter)
                return 'Brunoocto\Vmodel\JsonApi\VmodelAdapterSoftDeletes';
            }
        }
        return 'Brunoocto\Vmodel\JsonApi\Vmodel'.$unit;
    }
}
