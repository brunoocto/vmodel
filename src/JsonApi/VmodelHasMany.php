<?php

namespace Brunoocto\Vmodel\JsonApi;

use CloudCreativity\LaravelJsonApi\Eloquent\HasMany;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations;
use Neomerx\JsonApi\Contracts\Encoder\Parameters\EncodingParametersInterface;

/**
 * Class VmodelHasMany
 *
 * We extend HasMany because sync() does not take into account pivot additional values in the logic.
 * Note: add() seems not been used in our RESTful logic, but it might have the same issue as update(), need to observe if such bug appear.
 */
class VmodelHasMany extends HasMany
{
    /**
     * We overwrite the function to make sure it takes into account the pivot values while synchronizing
     *
     * @param Model $record
     * @param array $relationship
     * @param EncodingParametersInterface $parameters
     * @return void
     */
    public function update($record, array $relationship, EncodingParametersInterface $parameters)
    {
        // Get the type of relation (should usually be BelongsToMany here)
        $relation = $this->getRelation($record, $this->key);
        
        if ($relation instanceof Relations\BelongsToMany) {
            // Get the collection of all related objects
            $related = $this->findRelated($record, $relationship);
            // The array to send to sync
            $sync_array = [];
            // Check that the relationship data exists
            if (isset($relationship['data']) && is_array($relationship['data'])) {
                // Make sure that we build only relation that exists
                foreach ($related as $item) {
                    // Build the list with pivot values
                    foreach ($relationship['data'] as $value) {
                        // INFO: $this->key is the method name (nonLinears), $this->field is the relationship name
                        $relation_field = $record::getModelRelationship($this->field);
                        // If the relationship does not exists, we skip
                        if (!$relation_field) {
                            continue;
                        }
                        $relation_table = (new $relation_field[4])->getTable();
                        // We check that type matches
                        if ($value['type'] == $relation_table && $value['id'] == $item->getKey()) {
                            // Check if the addtional data are stored in a meta field
                            if (isset($value['meta']) && is_iterable($value['meta'])) {
                                // Make sure that we pass an array, not an object
                                $sync_array[$value['id']] = (array)$value['meta'];
                            } else {
                                $sync_array[$value['id']] = $value;
                                // Remove 'type' and 'id', because they are not part of additional pivot values
                                unset($sync_array[$value['id']]['type']);
                                unset($sync_array[$value['id']]['id']);
                            }
                        }
                    }
                }
            }
            
            // Synchronize with pivot values (note: this is where the original behavior was mistaken because it forgot to add pivot values)
            $relation->sync($sync_array);
            // Avoid to call the parent method
            return;
        }

        parent::update($record, $relationship, $parameters);
    }
}
