<?php

namespace Brunoocto\Vmodel\JsonApi;

use Brunoocto\Vmodel\JsonApi\VmodelResolver;

// https://laravel-json-api.readthedocs.io/en/latest/features/resolvers/
class VmodelResolverCreator
{
    /**
     * Creator
     *
     */
    public function __invoke($apiName, array $config)
    {
        return new VmodelResolver($config['resources']);
    }
}
