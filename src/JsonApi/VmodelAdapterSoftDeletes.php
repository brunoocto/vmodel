<?php

namespace Brunoocto\Vmodel\JsonApi;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Builder;
use Brunoocto\Vmodel\JsonApi\VmodelAdapter;
use CloudCreativity\LaravelJsonApi\Eloquent\Concerns\SoftDeletesModels;

class VmodelAdapterSoftDeletes extends VmodelAdapter
{
    /**
     * Because VmodelAbstract uses SoftDeletes trait.
     *
     */
    use SoftDeletesModels;

    /**
     * Add Customized filters for SoftDelete Models
     * Filters works only for Collection of items
     * For instance:
     *  /sample/pm/projects?filter[with-trashed]=true
     *
     * @param Builder $query
     * @param Collection $filters
     * @return void
     */
    protected function filter($query, Collection $filters)
    {
        // It includes soft-deleted resources in filter results.
        if ($filters->get('with-trashed') == true) {
            $query->withTrashed();
        } elseif ($filters->get('only-trashed') == true) {
            $query->onlyTrashed();
        }

        parent::filter($query, $filters);
    }

    /**
     * Override behavior of the trait SoftDeletesModels by using default behavior
     * It enables the Soft deletion instead of Hard deletion
     *
     * @param Model $record
     * @return bool
     */
    protected function destroy($record)
    {
        return parent::destroy($record);
    }

    /**
     * Override behavior of the trait SoftDeletesModels by using default behavior
     * It enables to exclude by default Soft deleted items
     *
     * @param $resourceId
     * @return Builder
     */
    protected function findQuery($resourceId)
    {
        return parent::findQuery($resourceId);
    }
}
