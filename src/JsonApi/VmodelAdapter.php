<?php

namespace Brunoocto\Vmodel\JsonApi;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Builder;
use Brunoocto\Vmodel\Models\VmodelModel;
use Brunoocto\Vmodel\JsonApi\VmodelHasMany;
use CloudCreativity\LaravelJsonApi\Routing\Route;
use CloudCreativity\LaravelJsonApi\Eloquent\AbstractAdapter;
use CloudCreativity\LaravelJsonApi\Pagination\StandardStrategy;
use CloudCreativity\LaravelJsonApi\Exceptions\RuntimeException;
use CloudCreativity\LaravelJsonApi\Utils\Str;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Neomerx\JsonApi\Contracts\Encoder\Parameters\EncodingParametersInterface;
use CloudCreativity\LaravelJsonApi\Contracts\Adapter\RelationshipAdapterInterface;

class VmodelAdapter extends AbstractAdapter
{
    /**
     * Map the relation methods from Eloquent to Adapter
     * https://laravel-json-api.readthedocs.io/en/stable/basics/adapters/
     *
     */
    protected static $eloquent_to_adapter_methods = [
        'hasOne' => 'hasOne',
        'hasOneThrough' => 'hasOneThrough',
        'belongsTo' => 'belongsTo',
        'hasMany' => 'hasMany',
        'belongsToMany' => 'hasMany',
        'hasManyThrough' => 'hasManyThrough',
        'morphTo' => 'belongsTo',
        'morphOne' => 'hasOne',
        'morphMany' => 'morphMany', // The online documentation was mistaken here (they said hasMany)
        'morphToMany' => 'hasMany',
        'morphedByMany' => 'morphMany',
    ];

    /**
     * GET parameters
     *
     * @var \Neomerx\JsonApi\Encoder\Parameters\EncodingParameters
     */
    protected $parameters;

    /**
     * Convert Elquent method to Adapter method
     *
     * @return string|null
     */
    public static function convertRelationMethod(string $method)
    {
        // Get the converted relationships method name
        if (isset(static::$eloquent_to_adapter_methods[$method])) {
            $method = static::$eloquent_to_adapter_methods[$method];
        }
        return $method;
    }


    /**
     * Variable to be used only at constructor.
     * It avoid to use multiple file/path and creating an Adapater per Model
     *
     */
    protected static $model_class_constructor = null;

    /**
     * Return the result for query that is not paginated.
     *
     * @param Builder $query
     * @return mixed
     */
    protected function searchAll($query)
    {
        // Only while doing multi-request like "/files", Eager load to save number of SQL queries while building relationships
        $query = $this->eagerLoading($query);
        return parent::searchAll($query);
    }

    /**
     * Eager load visible relationships to reduce number of calls especially in the case of multiple get
     *
     * @param  mixed $query
     * @return void
     */
    protected function eagerLoading($query)
    {
        // Prepare the list of relationships to attach
        $with = [];
        // Get the list of Visible relationships
        $relationships = $this->model::getPublicModelRelationships();
        foreach ($relationships as $relation) {
            // If the relation is visible
            if ($relation[1]) {
                // For morphTo, eager loading is different, see the documentation
                // https://laravel.com/docs/6.x/eloquent-relationships#eager-loading
                if ($relation[3] == 'morphTo') {
                    $child_relationships = $relation[4]::getModelRelationships();
                    foreach ($child_relationships as $child_relation) {
                        if ($child_relation[4] == $relation[4]) {
                            $query->with([$relation[2] => function (MorphTo $morphTo) use ($relation, $child_relation) {
                                $morphTo->morphWith([
                                    // Class name => [ hasMany method name in relationship class ]
                                    $relation[4] => [$child_relation[2]],
                                ]);
                            }]);
                            break;
                        }
                    }
                } else {
                    $with[] = $relation[2];
                }
            }
        }
        if (!empty($with)) {
            // Use eager loading
            $query->with($with);
        }

        return $query;
    }

    /**
     * We use VmodelHasMany instead of HasMany to be able to take into account pivot values
     *
     * @param string|null $modelKey
     * @return VmodelHasMany
     */
    protected function hasMany($modelKey = null)
    {
        return new VmodelHasMany($modelKey ?: $this->guessRelation());
    }

    /**
     * Filter the query
     * ISSUE: Because of $cats, some value displayed on front might be in different format.
     * The most relevant example is Date, we send timestamp while database uses Datetime.
     * We handle here the generic_casts, but for other casts in Models themselves, we may
     * change it from the client side which is not the best practive, but a workaround until
     * we find a reliable uncast method
     *
     * @param Builder $query
     * @param Collection $filters
     * @return void
     */
    protected function filter($query, Collection $filters)
    {
        /**
         * List of available field operators
         *
         * 0 => "="
         * 1 => "<"
         * 2 => ">"
         * 3 => "<="
         * 4 => ">="
         * 5 => "<>"
         * 6 => "!="
         * 7 => "<=>"
         * 8 => "like"
         * 9 => "like binary"
         * 10 => "not like"
         * 11 => "ilike"
         * 12 => "&"
         * 13 => "|"
         * 14 => "^"
         * 15 => "<<"
         * 16 => ">>"
         * 17 => "rlike"
         * 18 => "not rlike"
         * 19 => "regexp"
         * 20 => "not regexp"
         * 21 => "~"
         * 22 => "~*"
         * 23 => "!~"
         * 24 => "!~*"
         * 25 => "similar to"
         * 26 => "not similar to"
         * 27 => "not ilike"
         * 28 => "~~*"
         * 29 => "!~~*"
         *
         */
        
        // Group all requests by dividing them by OR, like: (A=1 & B=2) || (A=3 & C=4)
        // To add an OR condition, just add filter[or] in the GET parameters: /projects?filter[A]=1&filter[B]=2&filter[or]&filter[A(1)]=3&filter[C]=4
        $groups = [];
        // Groupnumber
        $group_number = 0;
        //check if at least one filter is present
        if (count($filters)>0) {
            // Get the list of permitted operator for the query
            $permitted_operators = $query->getQuery()->operators;
            // Add "in" condition ( filter[id,in]=1,42,88 )
            $permitted_operators[] = 'in';
            // Get available colunms to filter
            $columns = $this->model->getColumns();
            // Build groups
            foreach ($filters as $key => $value) {
                // Advanced: To enable multiple condition of the Field or OR statement, we can use numerical suffix like this:
                // ?filter[title]=abc&filter[title(1)]=def
                // ?filter[title]=abc&filter[or]&filter[title(1)]=def&filter[or(1)]&filter[title(2)]=ghi
                // Clean the key: or(2) => or
                $key = preg_replace('/\(\d\)/', '', $key);
                if ($key == 'or') {
                    // Initialize the next another group
                    $group_number++;
                    continue;
                } else {
                    /**
                     * Filter can use in following formats:
                     *  - filter[title]=test
                     *  - filter[title,'<>']=test
                     *  - filter[title,"<>"]=test
                     *  - filter[title, "<>"]=test
                     *  - filter[title,%3C%3E]=test (URL encoded version, prefered)
                     */
                    $explode = explode(',', $key);
                    // If the filter is empty, we skip
                    if (count($explode)<=0) {
                        continue;
                    }
                    // Get the field
                    $field = $explode[0];
                    // Check if the attribute exists
                    if (!in_array($field, $columns)) {
                        // if not we skip to next iteration
                        continue;
                    }
                    $operation = null;
                    if (isset($explode[1])) {
                        // Delete extra spaces
                        $operation = trim($explode[1]);
                        // Delete quotes and doubel quotes if any
                        $operation = preg_replace(['/^\"(.*)\"$/', '/^\'(.*)\'$/'], '$1', $operation);
                        // Check if the operation is permitted
                        if (!in_array($operation, $permitted_operators)) {
                            // If not, we skip
                            continue;
                        }
                    }
                    // We don't need to check here if the value follow the rules, it is already done in VmodelValidators->queryRules
                    // Initialize the group
                    if (!isset($groups[$group_number])) {
                        $groups[$group_number] = [];
                    }
                    // We convert the value to array if "in" condition
                    if ($operation == 'in') {
                        $value = array_map('trim', explode(',', $value));
                        // We revert the cast of value
                        foreach ($value as $i => $val) {
                            $value[$i] = $this->model->uncast($field, $val);
                        }
                    } else {
                        // We revert the cast of value
                        $value = $this->model->uncast($field, $value);
                    }
                    // We build the group used by the query
                    $groups[$group_number][] = [$operation, $field, $value];
                }
            }

            // We first wrap all conditions into an AND statement
            $query = $query
                ->where(function ($query) use ($groups) {
                    // We build the query according of the filters groups
                    foreach ($groups as $group) {
                        // For each group, we wrap them into an OR statement
                        $query = $query
                            ->orWhere(function ($query) use ($group) {
                                foreach ($group as $field => $info) {
                                    // If the operation is not specified, we assume that it is a "="
                                    $operation = $info[0];
                                    $field = $info[1];
                                    $value = $info[2];
                                    if (is_null($operation)) {
                                        // This can be something like: ->where('title', 'My title')
                                        $query = $query->where($field, $value);
                                    } elseif ($operation == 'in') {
                                        // This can be something like: ->whereIn('id', [1,42,88])
                                        $query = $query->whereIn($field, $value);
                                    } else {
                                        // This can be something like: ->where('title', '<>',  'My title')
                                        $query = $query->where($field, $operation, $value);
                                    }
                                }
                            });
                    }
                });
        }
        return $query;
    }

    /**
     * Fill relationships from a resource object.
     *
     * @param $record
     * @param Collection $relationships
     * @param EncodingParametersInterface $parameters
     * @return void
     */
    protected function fillRelationships(
        $record,
        Collection $relationships,
        EncodingParametersInterface $parameters
    ) {
        // By rewriting the method, we skip the check in AbstractResourceAdapter.php of the method existing to make it dynamic
        $relationships->each(function ($value, $field) use ($record, $parameters) {
            $this->fillRelationship($record, $field, $value, $parameters);
        });
    }

    /**
     * Overload the trait IncludesModels logic whihc does only camelCase convertion.
     * This method match the method name according to the relationships.
     * Which means "jobs" display match will match "tasks" method.
     *
     * @param string $field
     *      the JSON API field name.
     * @return string
     *      the expected relation name on the Eloquent model.
     */
    protected function modelRelationForField($field)
    {
        // Get all relationships we can display
        $relationships = $this->model::getPublicModelRelationships();
        if (isset($relationships[$field])) {
            // Get relation method name
            $field = $relationships[$field][2];
        }
        return $this->camelCaseRelations ? Str::camelize($field) : Str::underscore($field);
    }

    /**
     * Add relationships of relationships
     * These decrease the number of SQL queries for included relationships
     *
     * @param mixed $result Collection of entities
     * @return void
     */
    protected function setScopeIncludedRelationships($parameters)
    {
        // Get parameters
        $this->parameters = $this->getQueryParameters($parameters);

        if (!is_null($this->parameters)) {
            $include_paths = $this->parameters->getIncludePaths();
            if (!empty($include_paths)) {
                // Get all relationships we can display
                $relationships = $this->model::getPublicModelRelationships();
                foreach ($include_paths as $include_path) {
                    if (isset($relationships[$include_path])) {
                        // Get relation method name
                        $relationship = $relationships[$include_path][2];
                        // Get relation class
                        $relation_class = $relationships[$include_path][4];
                        // Get sub relation methods' name
                        $sub_relationships = array_column($relation_class::getPublicModelRelationships(), 2);
                        foreach ($sub_relationships as $sub_relationship) {
                            // Add closure of sub-relationship
                            $this->addClosureScope(function ($query) use ($relationship, $sub_relationship) {
                                $query->with($relationship.'.'.$sub_relationship);
                            });
                        }
                    }
                }
            }
        }
    }

    /**
     * Keep track of GET parameters
     *
     * @param EncodingParametersInterface $parameters
     * @return EncodingParametersInterface
     */
    protected function getQueryParameters(EncodingParametersInterface $parameters)
    {
        // Since it might be called multiple times, we only run it once
        if (is_null($this->parameters)) {
            $this->parameters = parent::getQueryParameters($parameters);
        }
        return $this->parameters;
    }

    /**
     * Adapter constructor.
     *
     * @param VmodelModel $model
     * @param StandardStrategy $paging (cannot use PagingStrategyInterface here, not instantiable)
     * @param Route $route
     *
     * @return void
     */
    public function __construct(VmodelModel $model, StandardStrategy $paging, Route $route)
    {
        if (!is_null(static::$model_class_constructor)) {
            // Use the class stored for constructor
            $resource_class = static::$model_class_constructor;
        } else {
            // Get the resource class ( /{resource}/{record} )
            // Note: POST/PATCH/DELETE only use Resource, so we can use getType (the class of the resource targeted)
            $resource_class = $route->getType();
        }
        // Instanciate the resource
        if (class_exists($resource_class)) {
            $model = new $resource_class();
        }

        // Reinitialize the variable
        static::$model_class_constructor = null;

        // Launch the Adapter
        parent::__construct($model, $paging);
    }

    /**
     * Set a variable to be use in the constructor.
     *
     * @param string $resource_class
     *
     * @return void
     */
    public static function setModelClassConstructor(string $resource_class)
    {
        if (class_exists($resource_class)) {
            static::$model_class_constructor = $resource_class;
        }
    }

    /**
     * Get the relation
     */
    public function getRelated(string $field): RelationshipAdapterInterface
    {
        // From $field (display name), get the method name, then apply Adapter method name with the method name as parameter
        // Check if the field exists as a relationship with the model
        if (!$relationship = $this->model::getModelRelationship($field)) {
            throw new RuntimeException("No existing relationship for field {$field}.");
        }

        // The Name of the relationship method
        $method_name = $relationship[2];
        // The display name of the relationship
        $relation_type = $relationship[3];

        // Convert the relation name
        $relation_type = static::convertRelationMethod($relation_type);

        if ($relation_type == 'morphMany') {
            // If the relation is morphMany only, the setup is different
            $relation = $this->morphMany(
                $this->hasMany($method_name)
            );
        } else {
            $relation = $this->{$relation_type}($method_name);
        }

        // Add the route relationship name
        $relation->withFieldName($field);

        return $relation;
    }

    /**
     * Check first if the method exists as a relation in the Model
     *
     * @param string $field the JSON API field name.
     * @return string|null the adapter's method name, or null if none is implemented.
     */
    protected function methodForRelation($field)
    {
        // From $field (display name), get the method name, then apply Adapter method name with the method name as parameter
        // Check if the field exists as a relationship with the model
        if ($relationship = $this->model::getModelRelationship($field)) {
            // The Name of the relationship method
            $field = $relationship[2];
        }

        // Check the relation method name
        if ($this->model::isRelationMethod($field)) {
            return $field;
        }

        return parent::methodForRelation($field);
    }

    /**
     * Enable The Adapter to return the Model relation method.
     * It help to dynamically build relation methods like ->tasks(), ->projects(), etc.
     * NOTE: It seems that it is not used because we overwrote getRelated method,
     * but I keep it because the documentation specify it
     * https://laravel-json-api.readthedocs.io/en/latest/basics/adapters/
     *
     * @param string $method Method name we try to call
     * @param array $args Parameters to pass the the method called
     * @return mixed
     */
    public function __call($method, $args)
    {
        // Check if the relation method name exists in the Model
        if ($this->model::isRelationMethod($method)) {
            return call_user_func_array($this->model->$method, $args);
        }
    }

    /**
     * Override the query to gather relationships of any "include"
     *
     * @param EncodingParametersInterface $parameters
     * @return mixed
     */
    public function query(EncodingParametersInterface $parameters)
    {
        // Add scope to retreive included relationships
        $this->setScopeIncludedRelationships($parameters);

        // Call parent method
        return parent::query($parameters);
    }

    /**
     * Override the query to gather relationships of any "include"
     *
     * @param Relations\BelongsToMany|Relations\HasMany|Relations\HasManyThrough|Builder $relation
     * @param EncodingParametersInterface $parameters
     * @return mixed
     */
    public function queryToMany($relation, EncodingParametersInterface $parameters)
    {
        // Add scope to retreive included relationships
        $this->setScopeIncludedRelationships($parameters);

        // Call parent method
        return parent::queryToMany($relation, $parameters);
    }

    /**
     * Override the query to gather relationships of any "include"
     *
     * @param Relations\BelongsToMany|Relations\HasMany|Relations\HasManyThrough|Builder $relation
     * @param EncodingParametersInterface $parameters
     * @return mixed
     */
    public function queryToOne($relation, EncodingParametersInterface $parameters)
    {
        // Add scope to retreive included relationships
        $this->setScopeIncludedRelationships($parameters);

        // Call parent method
        return parent::queryToOne($relation, $parameters);
    }

    /**
     * Override the query to gather relationships of any "include"
     *
     * @param mixed $record
     * @param EncodingParametersInterface $parameters
     * @return mixed
     */
    public function read($record, EncodingParametersInterface $parameters)
    {
        // Add scope to retreive included relationships
        $this->setScopeIncludedRelationships($parameters);

        // Apply the scope to a new query
        $this->applyScopes(
            $query = $record->newQuery()
        );

        // Get the record with all relationships
        $id = $record->getKey();
        $record = $query->where('id', $id)->first();

        // Call parent method
        return parent::read($record, $parameters);
    }

    /**
     * Override the query to gather relationships of any "include"
     *
     * @param mixed $record
     * @param EncodingParametersInterface $parameters
     * @return mixed
     */
    public function update($record, array $document, EncodingParametersInterface $parameters)
    {
        // Add scope to retreive included relationships
        $this->setScopeIncludedRelationships($parameters);

        // Apply the scope to a new query
        $this->applyScopes(
            $query = $record->newQuery()
        );

        // Get the record with all relationships
        $id = $record->getKey();
        $record = $query->where('id', $id)->first();

        // Call parent method
        return parent::update($record, $document, $parameters);
    }
}
