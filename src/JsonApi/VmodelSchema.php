<?php

namespace Brunoocto\Vmodel\JsonApi;

use Illuminate\Database\Eloquent\Collection;
use CloudCreativity\LaravelJsonApi\Routing\Route;
use Neomerx\JsonApi\Schema\SchemaProvider;
use Neomerx\JsonApi\Contracts\Schema\SchemaFactoryInterface;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Brunoocto\Vmodel\Models\VmodelModel;

// https://github.com/neomerx/json-api/wiki/Schemas
class VmodelSchema extends SchemaProvider
{

    /**
     * Variable to be used only at constructor.
     * It avoid to use multiple file/path and creating an Adapater per Model
     *
     */
    protected static $type_class_constructor = null;

    /**
     * Set a variable to be use in the constructor.
     *
     * @param string $resource_class
     *
     * @return void
     */
    public static function setTypeClassConstructor(string $resource_type)
    {
        if (config('json-api-brunoocto-vmodel.resources.'.$resource_type)) {
            static::$type_class_constructor = $resource_type;
        }
    }

    /**
     * Schema constructor.
     *
     * @param SchemaFactoryInterface $factory
     * @param Route $route
     * @return void
     */
    public function __construct(SchemaFactoryInterface $factory, Route $route)
    {
        if (!is_null(static::$type_class_constructor)) {
            // Use the type stored for constructor
            $this->resourceType = static::$type_class_constructor;
        } else {
            // Get from the route first the relationship type if it does exist
            $this->resourceType = $route->getInverseResourceType();
            if (is_null($this->resourceType)) {
                // If no relationshi defined in the URL, use the resource
                $this->resourceType = $route->getResourceType();
            }
        }

        // Reinitialize the constructor variable
        static::$type_class_constructor = null;

        parent::__construct($factory);
    }

    /**
     * Add pivot value to meta data
     * @param $resource
     *      the domain record being serialized.
     * @return array Array of pivot values
     */
    public function getLinkageMeta($resource)
    {
        // We return null if no pivot
        if (!isset($resource->pivot) || !$resource->pivot instanceof Pivot) {
            return null;
        }

        // Prepare the list of pivot values
        $list = [];
        foreach ($resource->pivot->getAttributes() as $key => $value) {
            // If not one of the 3 keys
            if ($key != $resource->pivot->getRelatedKey() && $key != $resource->pivot->getForeignKey() && $key != 'other_type') {
                // We add the additional pivot value to display it
                $list[$key] = $value;
            }
        }

        // If the array is empty we return null, if not we return the array of pivot value
        return empty($list) ? null : $list;
    }

    /**
     * Add the link to included ressources
     * @param $resource
     *      the domain record being serialized.
     * @return array Array of pivot values
     */
    public function getIncludedResourceLinks($resource)
    {
        // Use the same format as main resource
        return $this->getResourceLinks($resource);
    }

    /**
     * Add meta to the ressources
     * @param $resource
     * @return array
     */
    public function getPrimaryMeta($resource)
    {
        // This timestamp is to track when we use ->all() or ->latest(timestamp)
        // latest(timestamp) should include deleted items to delete them in the client side
        return ['checktime' => time()];
    }

    /**
     * Add meta to the included ressources
     * @param $resource
     * @return array
     */
    public function getInclusionMeta($resource)
    {
        return $this->getPrimaryMeta($resource);
    }

    /**
     * @param $resource
     *      the domain record being serialized.
     * @return string
     */
    public function getId($resource)
    {
        return (string) $resource->getRouteKey();
    }

    /**
     * Overwrite the function to make sure the Type is taking the on from the model
     *
     * @return string
     */
    public function getResourceType()
    {
        // Check if the resources is registered
        if ($resource_type = config('json-api-brunoocto-vmodel.resources.'.$this->resourceType)) {
            // Check if the clss extends of VmodelModel
            if (is_subclass_of($resource_type, VmodelModel::class)) {
                // Return the real type name (=table name)
                return (new $resource_type)->getTable();
            }
        }
        return $this->resourceType;
    }

    /**
     * @param $resource
     *      the domain record being serialized.
     * @return array
     */
    public function getAttributes($resource)
    {
        $attributes = $resource->toArray();
        // 'type' and 'id' are reserved keys
        unset($attributes['type']);
        unset($attributes['id']);
        return $attributes;
    }

    /**
     * Get resource links.
     *
     * @param object $resource
     * @param bool   $isPrimary
     * @param array  $includeRelationships A list of relationships that will be included as full resources.
     *
     * @return array
     */
    public function getRelationships($resource, $is_primary, array $include_relationships)
    {
        // Prepare the list of relationships
        $list = [];
        // Get all relationships we can display
        $relationships = $resource::getPublicModelRelationships();
        foreach ($relationships as $key => $relation) {
            // For morpthTo relation, because only one relationship is true, we can skip if the table name does not match to avoid a SQL call
            if ($relation[3] == 'morphTo' && isset($resource->other_type) && (new $relation[4])->getTable() != $resource->other_type) {
                continue;
            }
            // Get the method to call
            $method = $relation[2];
            // Retrieve only type and id
            $items = $resource->$method;

            // We count how many relationships
            $count = 0;
            if ($items instanceof VmodelModel) {
                $count = 1;
            } elseif ($items instanceof Collection) {
                // Compare to ->count(), this avoid a SQL call
                $count = count($items);
            }

            // We do not display empty list
            if ($count <= 0) {
                continue;
            }

            // Get the table name from the 1st instance
            if ($items instanceof Collection) {
                // hasMany relationship
                $type = $items[0]->getTable();
            } else {
                // hasOne relationship
                $type = $items->getTable();
            }

            // Additional information about the relation
            $list[$key] = [
                // Relationships Link with attributes
                self::SHOW_SELF => true,
                // Relationships Link with attributes
                self::SHOW_RELATED => true,
                // The ID list of relationships
                self::DATA => function () use ($items, $type) {
                    // Initialized the resource
                    static::setTypeClassConstructor($type);
                    return $items;
                },
            ];
            // Additional information
            $list[$key][self::META] = [
                // Total count of relationships
                'total' => $count,
                // Information if the relationships is editable
                'editable' => $relation[0],
            ];
        }

        return $list;
    }
}
