<?php

namespace Brunoocto\Vmodel\JsonApi;

use CloudCreativity\LaravelJsonApi\Routing\Route;
use CloudCreativity\LaravelJsonApi\Validation\AbstractValidators;
use CloudCreativity\LaravelJsonApi\Contracts\Validation\ValidatorInterface;
use Brunoocto\Vmodel\Models\VmodelModel;

class VmodelValidators extends AbstractValidators
{
    /**
     * The include paths a client is allowed to request.
     *
     * @var string[]|null
     *      the allowed paths, an empty array for none allowed, or null to allow all paths.
     */
    protected $allowedIncludePaths = null;

    /**
     * The sort field names a client is allowed send.
     *
     * @var string[]|null
     *      the allowed fields, an empty array for none allowed, or null to allow all fields.
     */
    protected $allowedSortParameters = null;

    /**
     * The filters a client is allowed send.
     *
     * @var string[]|null
     *      the allowed filters, an empty array for none allowed, or null to allow all.
     */
    protected $allowedFilteringParameters = null;

    /**
     * Get resource validation rules.
     *
     * @param mixed|null $record
     *      the record being updated, or null if creating a resource.
     * @return mixed
     */
    protected function rules($record, array $data): array
    {
        // If no record (the model to update) given (happening while creating), we get the model class from Route facade
        if (is_null($record)) {
            // Get the record class, we can use first level "getType" only because we don't allow relationships update
            $record_class = app()->make(Route::class)->getType();
            // Instanciate the resource
            if (class_exists($record_class)) {
                $record = new $record_class();
            }
        }

        // Prepare the array of rules for Creation and Upldating
        $rules = [];
        // Check if the object is a VmodelModel instance
        if ($record instanceof VmodelModel) {
            // Get properties rules
            $rules = $record->getRules();
        }

        // NOTE: Verifying relationship existance can be done here, but for some unknown reason it does not always work, and may not use newQuery with its constraint.
        // So I used checkRelationships instead
        // $rules['field.id'] = ['exists:table,id'];

        return $rules;
    }

    /**
     * Get query parameter validation rules.
     * Used to apply rules on GEt parameters (filter, include, page, sort and fields)
     * https://laravel-json-api.readthedocs.io/en/stable/basics/validators/#defining-rules_2
     *
     * @return array Rules to GET parameters
     */
    protected function queryRules(): array
    {
        // Skip the rules check if any request is different as GET
        if (mb_strtolower(request()->getMethod()) != 'get') {
            return [];
        }

        // Get the class current requested
        $record_class = app()->make(Route::class)->getType();
        // Use the same rules as input
        $rules = $record_class::getRulesList();
        // Intialize the rebuild list of rules (without required)
        $new_rules = [];

        // Remove required for GET parameters
        foreach ($rules as $key => $rule) {
            $new_rule = preg_replace(['/^required$/', '/\|required$/', '/^required\|/', '/\|required\|/'], ['', '', '', '|'], $rule);
            // If the rule is not an empty string (which may happen if the field is 'required' only)
            if (!empty($new_rule)) {
                // Apply rules on filter only
                $new_rules['filter.'.$key] = $new_rule;
            }
        }

        // Return the list of rules to apply to the filter parameter
        return $new_rules;
    }

    /**
     * Overload the function with the check of Relationships existance and access
     *
     * @param array $document
     * @return ValidatorInterface
     */
    public function create(array $document): ValidatorInterface
    {
        $validator = parent::create($document);

        // Check the relationship
        $this->checkRelationships($document);

        return $validator;
    }

    /**
     * Overload the function with the check of Relationships existance and access
     *
     * @param VmodelModel $record
     * @param array $document
     * @return ValidatorInterface
     */
    public function update($record, array $document): ValidatorInterface
    {
        $validator = parent::update($record, $document);

        // Check the relationship
        $this->checkRelationships($document, $record);

        return $validator;
    }

    /**
     * Check if all relationships exists or are accessible by current session
     *
     * @param VmodelModel $model
     * @param Route $route
     *
     * @return void
     */
    protected function checkRelationships(array $document, $record = null)
    {
        // If no record (the model to update) given (happening while creating), we get the model class from Route facade
        if (is_null($record)) {
            // Get the record class, we can use first level "getType" only because we don't allow relationships update
            $record_class = app()->make(Route::class)->getType();
            // Instanciate the resource
            if (class_exists($record_class)) {
                $record = new $record_class();
            }
        }

        // Check that the relationship data exists
        if ($record instanceof VmodelModel && isset($document['data'])) {
            if (isset($document['data']['relationships'])) {
                foreach ($document['data']['relationships'] as $field => $relation) {
                    // Get the model relationship
                    $relationship = $record::getModelRelationship($field);
                    // If the relationship exists we check the entity existance or access
                    if ($relationship && isset($relation['data'])) {
                        // Get relation class
                        $class = $relationship[4];
                        $list = [];
                        // If this is a toMany relation
                        if (is_array($relation['data'])) {
                            $list = $relation['data'];
                        // If this is a toOne relation
                        } else {
                            $list[] = $relation['data'];
                        }
                        foreach ($list as $data) {
                            // Check that the fields are present
                            if (isset($data->type) && isset($data->id)) {
                                // Get the entity
                                $entity = $class::find($data->id);
                                // If the entity is not accessible, we generate an error
                                if (is_null($entity)) {
                                    return \LinckoJson::error(400, 'Unauthorized relationship: '.$data->type.' ['.$data->id.']');
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
