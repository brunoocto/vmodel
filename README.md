# Introduction<br>
<br>
Vmodel library used as a tuturial to explain how to build Lincko library for modern PHP Frameworks.<br>
The main target is Laravel 6.x, but such library can also be imported inside Symphony or other framework that uses composer.<br>
<br>
<br>
## Documentation<br>
<br>
Please refer to the online wiki:<br>
[wiki](https://gitlab.com/brunoocto/vmodel/wikis/home)
<br>
<br>
## Installation<br>
<br>
In your framework root directory, open composer.json and add the following repository:<br>
```json
    [...]
    "repositories": [
        [...]
        {
            "type": "vcs",
            "url":  "git@gitlab.com:brunoocto/vmodel.git"
        }
        [...]
    ]
    [...]
```
<br>
<br>
Then import the library via composer:<br>
```console
me@dev:# composer require brunoocto/vmodel:"~1.0"
```
<br>
<br>
## Tests<br>
<br>
Run all Feature tests, Unit tests, and generate a Coverage report:<br>
```console
me@dev:# phpunit
```
<br>
